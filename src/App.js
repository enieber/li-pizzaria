import React, { Component } from 'react';
import { ApolloProvider } from 'react-apollo';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter,
} from 'react-router-dom';

import {
  logout,
  hasAuthentication,
} from './auth/auth';
import apolloClient from './services/apollo';
import ProductPage from './product/ProductPage';
import DetailsProduct from './product/DetailsProduct';
import LoginPage from './auth/LoginPage';

class App extends Component {
  render() {
    return (
      <ApolloProvider
	client={apolloClient}>
	<Router>
        <React.Fragment>
	  <Route
	    exact
            path="/"
            component={ProductPage}
	  />
	  <Route
	    path='/products/:uid'
            component={DetailsProduct}
          />
        </React.Fragment>
      </Router>
     </ApolloProvider>	
    );
  }
}

export default App;
