import React from 'react';
import ImageGallery from 'react-image-gallery';
import styled from 'styled-components';

const ContainerGallery = styled.div`
  display: flex;
  flex-direction: column;
`;


const ImageShow = styled.img`
  max-width: 100%;
`;

export default class Galery extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { title } = this.props;

    const images = {
      x3: 'https://picsum.photos/1600/800',
      x2: 'https://picsum.photos/800/500',
      x1: 'https://picsum.photos/400/300',
    };

    return (
      <div>
	<picture>
	  <source
	    srcset={images.x1}
	    media="(max-width: 400px)"
	  />
	  <source
	    srcset={images.x2}
	    media="(max-width: 800px)"
	  />
	  <source
	    srcset={images.x3}
	    media="(max-width: 1000px)"
	  />
	  <ImageShow
	    src={images.x3}
	  />
	</picture>	
      </div>
    );
  }
}

