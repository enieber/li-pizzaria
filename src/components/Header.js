import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import ShoppingCard from './ShoppingCard';

const HeaderContainer = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 2em;
  align-items: center;
  min-height: 10vh;
  color: #fafaff; 
`;

const LinkStyled = styled(Link)`
  color: #fafaff;
  text-decoration: none;
`;

export default class Header extends React.PureComponent {
  render() {
    return (
      <HeaderContainer>
	<h1><LinkStyled to="/">Li Pizzaria</LinkStyled></h1>
      </HeaderContainer>
    );
  }
}
