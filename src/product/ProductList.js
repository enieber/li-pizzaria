import React from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const ButtonBuy = styled(Link)`
  text-decoration: none;
  padding: 0.5em;
  border-radius: 10px;
  background: #fff;
  color: #333;
`;

const ProductItem = styled.div`
  padding: 1em; 
`;

const Container = styled.div`
  color: #fafaff;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: 1em;
  margin-right: 1em;
`;

const ProductImg = styled.img`
  max-width: 85vw;
  max-height: 40vh;
`

class ProductList extends React.Component {
  render() {
    const { products } = this.props;
    if (products.loading) {
      return (
        <Container>
	  Carregando...
	</Container>
      );	  
    }

    return (
      <Container>
	{products.allProducts.map((product) => {
  	  return (
  	    <ProductItem>
	      <ProductImg
		src={product.image}
		alt={`image of ${product.description}`}
	      />
	      <h2>{product.title}</h2>
	      <h3>{`R$ ${product.value}`}</h3>
	      <ButtonBuy
		to={`/products/${product.id}`}>
		Comprar
	      </ButtonBuy>
            </ProductItem>
           )}
	)}
      </Container>
    );
  }
}

const ProductsQuery = gql`
  query {
    allProducts {
      id
      title
      value
      image
    }
  }
`;

export default graphql(ProductsQuery, { name: 'products' })(ProductList);

