import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import Header from '../components/Header';
import ProductItem from './ProductItem';


const Container = styled.div`
color: #fafaff;
display: flex;
flex-direction: column;
justify-content: center;
margin-left: 1em;
margin-right: 1em;
`;

const ProductImg = styled.img`
max-width: 85vw;
max-height: 40vh;
`

class DetailsProduct extends React.Component {
  render() {
    const idProduct = `${this.props.match.params.uid}`.toString();
    const params =  `id: "${idProduct}"` 
    
    return (
      <Container>
       <Header/>
	<Query
	query={gql`
	  query {
	  Product(${params}) {
	    id
	    title
	    description
	    value
	    image
	    listImages
	  }
	}
      `}>
	{({ loading, error, data }) => {
	  if (loading) {
	    return (
	      <Container>
		Carregando...
	      </Container>
	    );	  
	  }	
	  
	  if (error) {
	    return (
	      <Container>
		Erro ao carregar o produto {idProduct}
	      </Container>
	    );
	  }

	  if (!data.Product) {
	    return (
	      <div>
		Produto não encontrado
	      </div>
	    );
	  }
	  
	  return (
	    <ProductItem
	      image={{
		src: data.Product.image,
		alt: data.Product.description,
	      }}
	      title={data.Product.title}
	      description={data.Product.description}
	      value={`R$ ${data.Product.value}`}
	    />
	  );
	}}
      </Query>
    </Container>
  );
  }
}

export default DetailsProduct;

