import React from 'react';
import {
  Redirect,
} from 'react-router-dom';

import {
  logout,
  hasAuthentication as hasToken
} from '../auth/auth';
import Header from '../components/Header';
import ProductList from './ProductList';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasToken: true,
    };
  }

  render() {
    return (
      <div>
        <Header
          logout={() => {
	    logout();
	    this.setState({ hasToken: false });
          }}
	/>
	<ProductList />
      </div>
    );
  }
}

