import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Container = styled.div`
  color: #fafaff;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: 1em;
  margin-right: 1em;
`;

const ProductImg = styled.img`
  max-width: 85vw;
  max-height: 40vh;
`

const ButtonBuy = styled(Link)`
  text-decoration: none;
  padding: 0.5em;
  border-radius: 10px;
  background: #fff;
  color: #333;
`;

export default class ProductItem extends React.Component {
  render() {
    const {
      image,
      title,
      description,
      value,
    } = this.props;

    return (
      <Container>
        <ProductImg
	  src={image.src}
	  alt={image.alt}
	/>
	<h2>{title}</h2>
	<p>{description}</p>
	<h3>{value}</h3>
	{this.props.button ? (this.props.button()) : (<div/>)}
      </Container>
    );
  }
}

